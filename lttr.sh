#!/usr/bin/env bash
 
######"listen-to-the-radio.sh" is a member of the //Quantifier//Consortium//. All rights reserved.

bold=$(tput bold)
natural=$(tput sgr0)
comment0="#EXTM3U"
comment1="##https://gitlab.com/llamakc/Lttr"
comment2="##Config file for the listen-to-the-radio music player $(date)"
red=`tput setaf 1`
reset=`tput sgr0`
#1="1"
#2="2"
##two separate comments is annoying, but necessary, since not all versions of echo respect the newline parameter, or the -e flag

if ! [ -x "$(command -v mpv)" ]; then
  echo "${bold}Oh, no! This script requires that you have mpv installed and available in your path. You should either install mpv, or edit the script to include your preferred player. ${natural}Exiting..." >&2
  exit 1
fi

mkdir -m700 -p /home/$USER/.config/lttr/
touch /home/$USER/.config/lttr/lttrrc;
chmod +x /home/$USER/.config/lttr/lttrrc;
clear;

#https://wiki.bash-hackers.org/howto/getopts_tutorial
#https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux


echo -ne  "${red}    ____________________________	\n"
echo -ne  "   |............................|	\n"
echo -ne  "   |:    gitlab.com/llamakc    :|	 \n"
echo -ne  "   |:       ${reset}  "LttR.sh" ${red}         :|	 \n"
echo -ne  "   |:     ,-.   _____   ,-.    :|	 \n"
echo -ne  "   |:    ( \`)) |_____| ( \`))   :|	\n"
echo -ne  "   |:     \`-\`   ' ' '   \`-\`    :| Which college rock \n"
echo -ne  "   |:     ,______________.     :| or NPR news station \n"
echo -ne  "   |...../::::o::::::o::::\.....| would you like to \n"
echo -ne  "   |..../:::O::::::::::O:::\....| listen to?\n"
echo -ne  "   |__/ /====/ /=//=/ /====/____/	\n\n ${reset}"

while getopts :s: flag
do
    case "${flag}" in
        s) username=${OPTARG};;
    esac
done
if [[ $username == ""  ]]; then

read -n 1 -p "
1=WVFS (Tallahassee)            2=WTUL (New Orleans)
3=WNYC (NPR New York)           4=WWNO (NPR New Orleans)
5=KEXP (Seatle)                 6=DR P6 Beat (Denmark)
7=Dandelion Radio (London)	8=KVRX (Austin)
9=WFUV (Bronx, NY)              0=Last Station Played
q=quit
==> ? " answer;

clear;

echo ""
echo " -- Happy Listening, $(whoami). The time and date right now is $(date) -- "

case $answer in

     0)

                echo " -- LttR is now happily playing for you the last station you listened to --";
                echo " --                  (Brought to you by Carl's Jr)                       --";
		echo " ";
		mpv --pulse-buffer=500 --cache=yes --cache-pause --playlist=/home/$USER/.config/lttr/lttrrc;;
      1)
		echo " -- Now you're listinging to WVFS, Tallahassee FL. You don't have to, but you are -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WVFS Tallahassee" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://voice.wvfs.fsu.edu:8000/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://voice.wvfs.fsu.edu:8000/stream --input-ipc-server=/tmp/mpvsocket;;
      2)
		echo " -- Now you're listinging to WTUL -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WTUL New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://129.81.156.83:8000/listen" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://129.81.156.83:8000/listen --input-ipc-server=/tmp/mpvsocket;;
      3)
		echo " -- Now you're listinging to your local NPR station WNYC -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WNYC New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://fm939.wnyc.org/wnycfm" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://fm939.wnyc.org/wnycfm --input-ipc-server=/tmp/mpvsocket;;
      4)
		echo " -- Now you're listinging to your local NPR station WWNO -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WWNO New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://tektite.streamguys1.com:5145/wwnolive" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://tektite.streamguys1.com:5145/wwnolive --input-ipc-server=/tmp/mpvsocket;;
      5)
		echo " -- Now you're listinging to KEXP, Seatle -- ";
		echo " -- Attempting to skip the commercials. Please hold...";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KEXP Seatle" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://kexp-mp3-128.streamguys1.com/kexp128.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --start=17 --force-seekable=yes --pulse-buffer=1000 --cache=yes --cache-pause https://kexp-mp3-128.streamguys1.com/kexp128.mp3 --input-ipc-server=/tmp/mpvsocket;;
 		############### https://kexp-mp3-128.streamguys1.com/kexp128.mp3
      6)
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
		echo " -- Now you're listinging to DR P6 all the way from Denmark -- ";
		echo " ";
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, DR P6 Denmark" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://live-icy.gslb01.dr.dk:80/A/A29H.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://live-icy.gslb01.dr.dk:80/A/A29H.mp3 --input-ipc-server=/tmp/mpvsocket;;
      d|D)
		echo " -- Now you're listinging to WUOG. Who here is missing that Golden Bowl?  -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WUOG Athens" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stream.wuog.org:8000" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stream.wuog.org:8000 --input-ipc-server=/tmp/mpvsocket;;
      8)
		echo " -- Now you're listinging to KVRX, staying weird in Austin TX -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KVRX Austin" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream --input-ipc-server=/tmp/mpvsocket;;
      9)
		echo " -- Now you're listinging to WFUV, Straight Outta The Bronx! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WFUV New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://onair.wfuv.org/onair-hi" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --start=15 --force-seekable=yes --pulse-buffer=999 --cache=yes --cache-pause http://onair.wfuv.org/onair-hi --input-ipc-server=/tmp/mpvsocket;;
      s|S)
		echo " -- This is WSOU; Seton Hall's Pirate Radio. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WSOU New Jersey" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://crystalout.surfernetwork.com:8001/WSOU_MP3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://crystalout.surfernetwork.com:8001/WSOU_MP3 --input-ipc-server=/tmp/mpvsocket;;
      7)
		echo " -- This is Dandelion Radio. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, Dandelio Radio London" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stream.dandelionradio.com:9414" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stream.dandelionradio.com:9414 --input-ipc-server=/tmp/mpvsocket;;
      p|P)
		echo " -- This is P6 Beat from Denmark. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, P6 Beat Denmark" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://live-icy.gslb01.dr.dk/A/A29H.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://live-icy.gslb01.dr.dk/A/A29H.mp3 --input-ipc-server=/tmp/mpvsocket;;
      c|C)
		echo " -- This is CJLO 1690 AM Montreal. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, CJLO 1690 AM Montreal" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://rosetta.shoutca.st:8883/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://rosetta.shoutca.st:8883/stream --input-ipc-server=/tmp/mpvsocket;;
      n|N)
		echo " -- This is DFM from the Netherlands (dfm.nl). A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, DFM Stereo Amsterdam" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stereo.dfm.nu" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stereo.dfm.nu --input-ipc-server=/tmp/mpvsocket;;
      r|R)
		echo " -- Here's a 90's Hip-Hop LoFi remix. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, 90's Hip_Hop Remix" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://www.youtube.com/watch?v=pKKIxeMIv78 --no-video" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv https://www.youtube.com/watch?v=pKKIxeMIv78 --no-video;;
      g|G)
		echo " -- Here's a 70's Soul remix. A Secret LttR station! What's goin' on?-- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, 90's Hip_Hop Remix" >> /home/$USER/.config/lttr/lttrrc;
                echo "mpv https://youtu.be/rSmf35baoSI --no-video" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv https://youtu.be/rSmf35baoSI --no-video;;
      t|T)
                /usr/bin/telnet towel.blinkenlights.nl;;
      w|W)
		echo " -- This is a playlist of local writing music. A Secret LttR station!                      -- ";
		echo " -- Did it fail for you? It's looking for local music saved as ~/data/.music/*mp3 -- ";
                echo " ";

                mpv --shuffle ~/data/.music/*mp3;;
esac

elif [ "$username" = 1 ]; then
 echo " -- First Flag: Now you're listinging to WVFS, Tallahassee FL. You don't have to, but you are -- ";
                echo " ";
                echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "#EXTINF:-1, WVFS Tallahassee" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://voice.wvfs.fsu.edu:8000/stream" >> /home/$USER/.config/lttr/lttrrc;
                exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://voice.wvfs.fsu.edu:8000/stream --input-ipc-server=/tmp/mpvsocket;
elif [ "$username" = 2 ]; then
  echo " -- Now you're listinging to WTUL -- ";
                echo " ";
                echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "#EXTINF:-1, WTUL New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://129.81.156.83:8000/listen" >> /home/$USER/.config/lttr/lttrrc;
                exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://129.81.156.83:8000/listen --input-ipc-server=/tmp/mpvsocket;
elif [ "$username" = 3 ]; then
                echo " -- Now you're listinging to your local NPR station WNYC -- ";
                echo " ";
                echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "#EXTINF:-1, WNYC New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://fm939.wnyc.org/wnycfm" >> /home/$USER/.config/lttr/lttrrc;
                exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://fm939.wnyc.org/wnycfm --input-ipc-server=/tmp/mpvsocket;
elif [ "$username" = 4 ]; then
echo " -- Now you're listinging to your local NPR station WWNO -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WWNO New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://tektite.streamguys1.com:5145/wwnolive" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://tektite.streamguys1.com:5145/wwnolive --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 5 ]; then
echo " -- Now you're listinging to KEXP, Seatle -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KEXP Seatle" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://live-mp3-128.kexp.org:8000/kexp128.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://live-mp3-128.kexp.org:8000/kexp128.mp3 --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 6 ]; then
echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
		echo " -- Now you're listinging to DR P6 all the way from Denmark -- ";
		echo " ";
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, DR P6 Denmark" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://live-icy.gslb01.dr.dk:80/A/A29H.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://live-icy.gslb01.dr.dk:80/A/A29H.mp3 --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 7 ]; then
echo " -- Now you're listinging to WUOG. Who here is missing that Golden Bowl?  -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WUOG Athens" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stream.wuog.org:8000" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stream.wuog.org:8000 --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 8 ]; then
echo " -- Now you're listinging to KVRX, staying weird in Austin TX -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KVRX Austin" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 9 ]; then
echo " -- Now you're listinging to WFUV, Straight Outta The Bronx! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WFUV New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://onair.wfuv.org/onair-hi" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://onair.wfuv.org/onair-hi --input-ipc-server=/tmp/mpvsocket;
fi

